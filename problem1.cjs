const fs = require('fs');
const path = require('node:path')

function problem1(numberOfFiles) {
    try {
        let pathForDirectory = '/home/pooja/callback/Directory1'
        fs.mkdir(pathForDirectory, function (error) {
            if (error) {
                console.log(error);
            } else {
                console.log("new Directory created")
                for (let index = 0; index < numberOfFiles; index++) {
                    let fileName = `randomFiles${index + 1}.json`
                    let filePath = path.join(pathForDirectory, fileName)
                    fs.writeFile(filePath, 'filecreating', function (error) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log("Create a file");
                            fs.unlink(filePath, function (error) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log("file deleted sucessfully");
                                }
                            })
                        }
                    })
                }
            }
        })
    } catch (error) {
        console.error(error);
    }
}

module.exports = problem1;