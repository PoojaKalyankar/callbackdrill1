const fs = require('fs');

function operations() {
    try {
        let sourcePath = '/home/pooja/callback/lipsum.txt'
        fs.readFile(sourcePath, 'utf8', function (error, data) {
            if (error) {
                console.log(error);
            } else {
                console.log("data read");
                let contentInUpper = data.toUpperCase();
                //console.log(contentInUpper);
                let pathForFile1 = './file1.txt'
                fs.writeFile(pathForFile1, contentInUpper, function (error) {
                    if (error) {
                        console.log(error);
                        return;
                    } else {
                        console.log("data convert to uppercase then file1 craeted");
                        let storeFileNames = './filenames.txt'
                        fs.appendFile(storeFileNames, './file1.txt\n', function (error) {
                            if (error) {
                                console.log(error);
                            } else {
                                console.log("file1 name added to filenames file");
                            }
                        })
                        let contentInLower = data.toLowerCase();
                        let splitting = contentInLower.split('.').join('\n');
                        let pathForFile2 = './file2.txt'
                        fs.writeFile(pathForFile2, splitting, function (error) {
                            if (error) {
                                console.log(error);
                                return;
                            } else {
                                fs.appendFile(storeFileNames, './file2.txt\n', function (error) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        console.log("file2 name added to filenames file");
                                    }
                                })
                                console.log("File convert to lowercase and file2 created");
                                fs.readFile(pathForFile2, 'utf8', function (error, data) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        // console.log(data);
                                        let dataForFile3 = data.split(",").sort().toString();
                                        let pathForFile3 = './file3.txt';
                                        fs.writeFile(pathForFile3, dataForFile3, function (error) {
                                            if (error) {
                                                console.log(error);
                                            } else {
                                                console.log("file3  created with sorted content");
                                                fs.appendFile(storeFileNames, './file3.txt\n', function (error) {
                                                    if (error) {
                                                        console.log(error);
                                                    } else {
                                                        console.log("file3 name added to filenames file");
                                                    }
                                                })
                                                fs.readFile(storeFileNames, 'utf8', function (error, filename) {
                                                    if (error) {
                                                        console.log(error);
                                                    } else {
                                                        let filenamesArray = filename.trim().split('\n');
                                                        filenamesArray.forEach(filename => {
                                                            fs.unlink(filename.trim(), function (error) {
                                                                if (error) {
                                                                    console.log(error);
                                                                    return;
                                                                }
                                                                console.log(`${filename} deleted`);
                                                            });

                                                        })
                                                    }
                                                })

                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                });
            }
        });
    } catch (error) {
        console.error(error);
    }
}


module.exports = operations;
